Slack Classroom
------------

A tiny web application to invite a user into your Slack team and simply the creation of private groups for the students and the join process.

Inspired by [https://github.com/outsideris/slack-invite-automation ](https://github.com/outsideris/slack-invite-automation )


Readme in progress...


# OAuth tokens
1. Visit <https://api.slack.com/apps> and Create New App.

    ![](https://raw.github.com/outsideris/slack-invite-automation/master/screenshots/oauth1.gif)

1. Click "Permissions".

    ![](https://raw.github.com/outsideris/slack-invite-automation/master/screenshots/oauth2.gif)

1. In "OAuth & Permissions" page, select `admin` scope under "Permission Scopes" menu and save changes.

    ![](https://raw.github.com/outsideris/slack-invite-automation/master/screenshots/oauth3.gif)

1. Click "Install App to Team".

    ![](https://raw.github.com/outsideris/slack-invite-automation/master/screenshots/oauth4.gif)

1. Visit <https://slack.com/oauth/authorize?&client_id=CLIENT_ID&team=TEAM_ID&install_redirect=install-on-team&scope=admin+client> in your browser and authorize it.
    * It authorizes the `client` permission. Otherwise, you can see `{"ok":false,"error":"missing_scope","needed":"client","provided":"admin"}` error.
    * Your `CLIENT_ID` could be found in "Basic Information" menu of your app page that you just install.
    * Your `TEAM_ID` could be found in <https://api.slack.com/methods/team.info/test>
