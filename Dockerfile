FROM node

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .
COPY package.json package-lock.json ./

RUN npm install

COPY . .

EXPOSE 80
CMD [ "npm", "start" ]