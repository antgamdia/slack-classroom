const express = require('express');
const request = require('request');
const passport = require('passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();

const router = express.Router();

const config = require('../config');


// ROUTERS AND TEMPLATE RENDERS
router.get('/', function (req, res) {
  res.render('index', {
    community: config.community,
    user: req.user,
    userProfile: JSON.stringify(req.user, null, '  ')
  });
});


// BEGIN AUTH0

router.get('/login', passport.authenticate('auth0', {
  clientID: config.auth0ClientId,
  domain: config.auth0Domain,
  redirectUri: "http://" + config.url + ":" + config.port + "/callback",
  responseType: 'code',
  audience: 'https://' + config.auth0Domain + '/userinfo',
  scope: 'openid profile'
}),
  function (req, res) {
    res.redirect("/");
  });

router.get('/logout', function (req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/failure'
  }),
  function (req, res) {
    res.redirect(req.session.returnTo || '/');
  }
);
router.get('/management-token', function (req, res) {
  var data = {
    url: "https://slack.com/api/oauth.access",
    form: {
      client_id: config.slacktokenManagementClientId,
      client_secret: config.slacktokenManagementSecret,
      code: req.query.code
    }
  };
  console.log("Requesting management token with data: " + JSON.stringify(data));
  request.post(data, function (e, r, body) {
    var bodyObj = JSON.parse(body);
    var access_token = bodyObj.access_token;
    console.log("Getting management token: " + access_token);
    if (access_token) {
      config.slacktokenManagement = access_token;
      console.log("Added management token " + config.slacktokenManagement);
    } else {
      console.log("Error, response body is " + body);
    }
    res.redirect('/');
  });
});

router.get('/autoinvite-token', function (req, res) {
  var data = {
    url: "https://slack.com/api/oauth.access",
    form: {
      client_id: config.slacktokenAutoinviteClientId,
      client_secret: config.slacktokenAutoinviteSecret,
      code: req.query.code
    }
  };
  console.log("Requesting autoinvite token with data: " + JSON.stringify(data));
  request.post(data, function (e, r, body) {
    var bodyObj = JSON.parse(body);
    var access_token = bodyObj.access_token;
    console.log("Getting autoinvite token: " + access_token);
    if (access_token) {
      config.slacktokenAutoinvite = access_token;
      console.log("Added autoinvite token " + config.slacktokenAutoinvite);
    } else {
      console.log("Error, response body is " + body);
    }
    res.redirect('/');
  });
});

router.get('/failure', function (req, res) {
  var error = req.flash("error");
  var error_description = req.flash("error_description");
  req.logout();
  res.render('error', {
    isFailed: true,
    data: {
      message: err.message,
      error: {
        error: error[0],
        error_description: error_description[0]
      }
    }
  });
});

/// END AUTH0

router.get('/groups', function (req, res) {
  doGetUsersMembership(req, res, function (members) {
    doGetGroups(req, res, function (groups) {
      res.render('groups', {
        community: config.community,
        members: members,
        groups: groups,
        user: req.user,
        userProfile: JSON.stringify(req.user, null, '  ')
      });
    });
  });
});

router.get('/admin', ensureLoggedIn, function (req, res) {
  var is_teacher = config.teachersId.includes(req.user._json.sub.split("|")[1]);
  if (is_teacher) {
    doGetUsersMembership(req, res, function (members) {
      doGetGroups(req, res, function (groups) {
        res.render('admin', {
          community: config.community,
          members: members,
          groups: groups,
          user: req.user,
          userProfile: JSON.stringify(req.user, null, '  ')
        });
      });
    });
  } else {
    res.redirect(req.session.returnTo || '/');
  }
});

router.get('/admin/config', ensureLoggedIn, function (req, res) {
  var is_teacher = config.teachersId.includes(req.user._json.sub.split("|")[1]);
  if (is_teacher) {
    res.send(config);
  } else {
    res.redirect(req.session.returnTo || '/');
  }
});

router.get('/invite', function (req, res) {
  res.render('invite', {
    community: config.community,
    user: req.user,
    userProfile: JSON.stringify(req.user, null, '  ')
  });
});

router.post('/invite', function (req, res) {
  doPostInvite(req, res, function (message) {
    res.render('result', {
      community: config.community,
      data: message.data,
      user: req.user,
      userProfile: JSON.stringify(req.user, null, '  ')
    });
  });
});

// API FUNCTIONS
router.get('/api/v1/members', function (req, res) {
  doGetUsers(req, res, function (data) {
    res.send(data);
  });
});

router.get('/api/v1/membership', function (req, res) {
  doGetUsersMembership(req, res, function (data) {
    res.send(data);
  });
});

router.get('/api/v1/groups', function (req, res) {
  doGetGroups(req, res, function (data) {
    res.send(data);
  });
});

router.post('/api/v1/invite', function (req, res) {
  doPostInvite(req, res, function (data) {
    res.send(data);
  });
});

router.post('/api/v1/createGroups', function (req, res) {
  doPostCreateGroups(req, res, function (data) {
    res.send(data);
  });
});

router.post('/api/v1/joinGroup', function (req, res) {
  doPostJoinGroup(req, res, function (data) {
    res.send(data);
  });
});

router.post('/api/v1/kickGroup', function (req, res) {
  doPostKickGroup(req, res, function (data) {
    res.send(data);
  });
});

// HANDLERS

function doPostJoinGroup(req, res, cb) {
  if (req.body.memberId && req.body.groupId) {

    request.post({
      url: 'https://' + config.slackUrl + '/api/groups.invite',
      form: {
        token: config.slacktokenManagement,
        channel: req.body.groupId,
        user: req.body.memberId
      }
    }, function (error, httpResponse, body) {
      if (error) {
        return cb({
          isFailed: true,
          data: error
        });
      }

      body = JSON.parse(body);

      if (!!body.ok) {
        return cb({
          isFailed: false,
          data: "Successfully added member " + req.body.memberId + " to " + req.body.groupId
        });

      } else {
        var err = body.error;
        if (err === 'to_be_done') { //TODO: set more cases according to the docs
          err = 'To be done.';
        }

        return cb({
          isFailed: true,
          data: err
        });
      }
    });

  } else {
    var errMsg = [];
    if (!req.body.memberId) {
      errMsg.push('memberId is required');
    }
    if (!req.body.groupId) {
      errMsg.push('groupId is required');
    }
    return cb({
      isFailed: true,
      data: errMsg.join(' and ') + '.'
    });
  }
}

function doPostKickGroup(req, res, cb) {
  if (req.body.memberId && req.body.groupId) {

    request.post({
      url: 'https://' + config.slackUrl + '/api/groups.kick',
      form: {
        token: config.slacktokenManagement,
        channel: req.body.groupId,
        user: req.body.memberId
      }
    }, function (error, httpResponse, body) {
      if (error) {
        return cb({
          isFailed: true,
          data: error
        });
      }

      body = JSON.parse(body);

      if (!!body.ok) {
        return cb({
          isFailed: false,
          data: "Successfully removed member " + req.body.memberId + " from " + req.body.groupId
        });

      } else {
        var err = body.error;
        if (err === 'to_be_done') { //TODO: set more cases according to the docs
          err = 'To be done.';
        }

        return cb({
          isFailed: true,
          data: err
        });
      }
    });

  } else {
    var errMsg = [];
    if (!req.body.memberId) {
      errMsg.push('memberId is required');
    }
    if (!req.body.groupId) {
      errMsg.push('groupId is required');
    }
    return cb({
      isFailed: true,
      data: errMsg.join(' and ') + '.'
    });
  }
}

function doPostCreateGroups(req, res, cb) {
  doGetUsers(req, res, function (members) {
    var students = members.data.filter(function (member) {
      return !member.is_teacher;
    });
    var teachers = members.data.filter(function (member) {
      return !!member.is_teacher;
    });

    var numOfGroups = Math.ceil(students.length / config.studentsPerGroup);
    var groupsToCreate = [];

    for (var i = 0; i < numOfGroups; i++) {
      var paddingDigit = (String(0).repeat(2) + String(i)).slice(String(i).length);
      groupsToCreate.push(config.groupPrefix + paddingDigit);
    }

    groupsToCreate.forEach(function (groupname) {
      request.post({
        url: 'https://' + config.slackUrl + '/api/groups.create',
        form: {
          token: config.slacktokenManagement,
          name: groupname
        }
      }, function (error, httpResponse, body) {
        if (error) {
          return cb({
            isFailed: true,
            data: error
          });
        }

        body = JSON.parse(body);

        if (!!body.group) {
          //  begin teacher inscription
          teachers.forEach(function (teacher) {

            var channel = body.group.id;
            var teacherId = teacher.id;

            request.post({
              url: 'https://' + config.slackUrl + '/api/groups.invite',
              form: {
                token: config.slacktokenManagement,
                channel: channel,
                user: teacherId
              }
            }, function (error, httpResponse, body) {
              if (error) {
                return cb({
                  isFailed: true,
                  data: error
                });
              }

              body = JSON.parse(body);

              if (!!body.ok) {

              } else {
                var err = body.error;
                if (err === 'to_be_done') { //TODO: set more cases according to the docs
                  err = 'To be done.';
                }

                return cb({
                  isFailed: true,
                  data: err
                });
              }
            });
          }, this);
          // end teacher inscription

        } else {
          var err = body.error;
          if (err === 'name_taken') {
            //pass
          } else {
            return cb({
              isFailed: true,
              data: err
            });
          }

        }
      });
    });
    return cb({
      isFailed: false,
      data: {
        msg: "Async operation in progress. Wait for a few seconds before calling another endpoint.",
        groups: groupsToCreate
      }
    });
  });
}

function doPostInvite(req, res, cb) {
  if (req.body.email && (!config.inviteToken || (!!config.inviteToken && req.body.token === config.inviteToken))) {
    request.post({
      url: 'https://' + config.slackUrl + '/api/users.admin.invite',
      form: {
        email: req.body.email,
        token: config.slacktokenAutoinvite, //we need a special token since users.admin.invite is an undocumented endpoint that needs an old scope to work
        set_active: true
      }
    }, function (error, httpResponse, body) {
      if (error) {
        return cb({
          isFailed: true,
          data: error
        });
      }

      body = JSON.parse(body);

      if (body.ok) {
        return cb({
          isFailed: false,
          data: 'Success! Check &ldquo;' + req.body.email + '&rdquo; for an invite from Slack.'
        });

      } else {
        var err = body.error;
        if (err === 'already_invited' || err === 'already_in_team') {
          err = 'You are already invited.';

        } else if (err === 'invalid_email') {
          err = 'The email you entered is an invalid email.';

        } else if (err === 'invalid_auth') {
          err = 'Something has gone wrong. Please contact a system administrator.';
        }

        if (err) {
          return cb({
            isFailed: true,
            data: err
          });
        }

      }
    });

  } else {
    var errMsg = [];
    if (!req.body.email) {
      errMsg.push('your email is required');
    }

    if (!!config.inviteToken) {
      if (!req.body.token) {
        errMsg.push('valid token is required');
      }

      if (req.body.token && req.body.token !== config.inviteToken) {
        errMsg.push('the token you entered is wrong');
      }
    }
    return cb({
      isFailed: true,
      data: errMsg.join(' and ') + '.'
    });
  }
}

function doGetUsers(req, res, cb) {
  request.post({
    url: 'https://' + config.slackUrl + '/api/users.list',
    form: {
      token: config.slacktokenManagement,
    }
  }, function (error, httpResponse, body) {
    if (error) {
      return cb({
        isFailed: true,
        data: error
      });
    }

    body = JSON.parse(body);

    if (!!body.members && body.members.length > 0) {
      var membersSent = body.members.filter(function (member) {
        return member.is_bot === false && member.is_restricted === false && member.is_ultra_restricted === false && member.is_app_user === false && member.deleted === false && member.name !== "slackbot";
      }).map(function (member) {
        var is_teacher = config.teachers.includes(member.profile.email);
        return {
          id: member.id,
          name: member.name,
          real_name: member.real_name,
          email: member.profile.email,
          is_teacher: is_teacher
        };
      });
      return cb({
        isFailed: false,
        data: membersSent
      });

    } else {
      var err = body.error;
      if (err === 'to_be_done') { //TODO: set more cases according to the docs
        err = 'To be done.';
      }

      return cb({
        isFailed: true,
        data: err
      });
    }
  });
}

function doGetUsersMembership(req, res, cb) {
  request.post({
    url: 'https://' + config.slackUrl + '/api/users.list',
    form: {
      token: config.slacktokenManagement,
    }
  }, function (error, httpResponse, body) {
    if (error) {
      return cb({
        isFailed: true,
        data: error
      });
    }

    body = JSON.parse(body);

    if (!!body.members && body.members.length > 0) {
      var membersSent = body.members.filter(function (member) {
        return member.is_bot === false && member.is_restricted === false && member.is_ultra_restricted === false && member.is_app_user === false && member.deleted === false && member.name !== "slackbot";
      });

      doGetGroups(req, res, function (groups) {
        var data = membersSent.map(function (member) {
          var is_teacher = config.teachers.includes(member.profile.email);

          var groupssent = groups.data.filter(function (group) {
            var gmembers = group.members.filter(function (gmember) {
              return member.id === gmember.id;
            });
            if (gmembers.length > 0) {
              return gmembers;
            }
          });

          groupssent = groupssent.map(function (group) {
            return {
              id: group.id,
              name: group.name
            }
          });

          return {
            id: member.id,
            name: member.name,
            real_name: member.real_name,
            email: member.profile.email,
            is_teacher: is_teacher,
            groups: groupssent
          };

        });

        return cb({
          isFailed: false,
          data: data
        });
      });



    } else {
      var err = body.error;
      if (err === 'to_be_done') { //TODO: set more cases according to the docs
        err = 'To be done.';
      }

      return cb({
        isFailed: true,
        data: err
      });
    }
  });
}

function doGetGroups(req, res, cb) {
  request.post({
    url: 'https://' + config.slackUrl + '/api/groups.list',
    form: {
      token: config.slacktokenManagement,
      exclude_archived: 1
    }
  }, function (error, httpResponse, body) {
    if (error) {
      return cb({
        isFailed: true,
        data: error
      });
    }

    body = JSON.parse(body);

    if (!!body.groups && body.groups.length > 0) {

      var users = doGetUsers(req, res, function (members) {
        var groupsSent = body.groups.filter(function (group) {
          return group.is_group === true && group.is_archived === false && group.name.toUpperCase().includes(config.groupPrefix.toUpperCase());
        }).map(function (group) {
          var membersSent = members.data.filter(function (member) {
            var filterMembers = group.members.filter(function (memberId) {
              return member.id === memberId;
            });
            if (filterMembers.length > 0) {
              return filterMembers;
            }
          });

          return {
            id: group.id,
            name: group.name,
            members: membersSent
          };
        });

        return cb({
          isFailed: false,
          data: groupsSent
        });
      });

    } else {
      var err = body.error;
      if (err === 'to_be_done') { //TODO: set more cases according to the docs
        err = 'To be done.';
      }

      return cb({
        isFailed: true,
        data: err
      });
    }
  });
}


module.exports = router;
